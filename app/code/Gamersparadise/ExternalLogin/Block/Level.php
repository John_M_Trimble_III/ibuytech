<?php

namespace Gamersparadise\ExternalLogin\Block;

class Level extends \Magento\Framework\View\Element\Template
{
	protected $_customerSession;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Customer\Model\Session $customerSession
	)
	{
		$this->_customerSession = $customerSession;
		parent::__construct($context);
	}

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
    	$customer = $this->_customerSession->getCustomer();
        
    	$gpLevel = $customer->getData('gp_level');
    	$gpPoints = $customer->getData('gp_points');

    	if ($this->_customerSession->isLoggedIn())
    	{
    		$message = "</br>Level: $gpLevel | Points: $gpPoints";
    		return $message;
    	}
    }
}