<?php

namespace Gamersparadise\ExternalLogin\Controller\Login;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session;

class Login extends Action
{   
	protected $_customerFactory;
	protected $_resultRedirect;
	protected $_storeManager;
	protected $_customerSession;

    public function __construct(
        Context $context,
        CustomerFactory $customerFactory,
        ResultFactory $resultFactory,
        StoreManagerInterface $storeManager,
		Session $customerSession        
    ) {
        parent::__construct($context);
        $this->_customerFactory = $customerFactory;
        $this->_resultRedirect = $resultFactory;
        $this->_storeManager = $storeManager;
        $this->_customerSession = $customerSession;
    }
    
    public function execute()
    {
		$params = $this->getRequest()->getParams();
		if(array_key_exists('email', $params) && array_key_exists('gp_token', $params))
		{
 			$customers = $this->_customerFactory->create()->getCollection()
 				->addAttributeToFilter("email", $params["email"])
				->addAttributeToFilter("gp_token", $params["gp_token"])
 				->load();
 			$customer = $customers->getFirstItem();
 			if($customer->getId())
 			{
 				$this->_customerSession->setCustomerAsLoggedIn($customer);
 			}
		}
    	$resultRedirect = $this->_resultRedirect->create(ResultFactory::TYPE_REDIRECT);
    	$resultRedirect->setUrl($this->_storeManager->getStore()->getBaseUrl());
    	return $resultRedirect; 		
    }
}
