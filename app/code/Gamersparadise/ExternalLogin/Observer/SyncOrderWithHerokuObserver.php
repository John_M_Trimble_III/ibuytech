<?php

namespace Gamersparadise\ExternalLogin\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order;
/**
 * Class SyncOrderWithHerokuObserver
 */
class SyncOrderWithHerokuObserver implements \Magento\Framework\Event\ObserverInterface
{
    protected $scopeConfig;
    protected $order;

    public function __construct(
        Order $order,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->order = $order;
    }

    /**
     * Execute observer.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // Get url of store
        // Get user token
        $orderids = $observer->getEvent()->getOrderIds();
        
        foreach($orderids as $orderid)
        {
            $order = $this->order->load($orderid);
        }


        $url = $this->scopeConfig->getValue('gamersparadise/general/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $apiToken = $this->scopeConfig->getValue('gamersparadise/general/user_token', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        //next example will insert new conversation
        $url .= 'api/v1/orders';
        $curl = curl_init($url);
        $curl_post_data = array( 
            "api_token" => "ByZ7pq6ooNtoNLs85kLZbzu",            
            "order" => array (
                "order_number" => $order->getIncrementId(),
                "total_value" => $order->getGrandTotal(), 
                "external_id" => $order->getId()
            ),
            "magento_user_id" => $order->getCustomerId()
         );


        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curl_post_data));
        $curl_response = curl_exec($curl);
                
        curl_close($curl);

        return $this;
    }
}