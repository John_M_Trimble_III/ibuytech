<?php

namespace Gamersparadise\ExternalLogin\Plugin;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FillInGpTokenPlugin
{
    public function beforeSave(
            \Magento\Customer\Model\Customer $customer
    ) {     
    	if(!$customer->getId() && !$customer->getGpToken())
    	{
    		$customer->setGpToken(bin2hex(random_bytes(52)));	
    	}
    	
    }
}

