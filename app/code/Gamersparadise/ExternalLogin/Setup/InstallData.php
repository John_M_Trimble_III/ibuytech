<?php 

namespace Gamersparadise\ExternalLogin\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;

use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class InstallData implements InstallDataInterface
    {
        protected $customerSetupFactory;

        /**
         * @var AttributeSetFactory
         */
        private $attributeSetFactory;

        /**
         * @param CustomerSetupFactory $customerSetupFactory
         * @param AttributeSetFactory $attributeSetFactory
         */
        public function __construct(
            CustomerSetupFactory $customerSetupFactory,
            AttributeSetFactory $attributeSetFactory
        ) {
            $this->customerSetupFactory = $customerSetupFactory;
            $this->attributeSetFactory = $attributeSetFactory;
        }

        public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
        {
            $setup->startSetup();


            /** @var CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

            $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();

            /** @var $attributeSet AttributeSet */
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

            $setup->getConnection()->addColumn(
                $setup->getTable('customer_entity'),
                'gp_token',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Gamer\s Paradise Token'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('customer_entity'),
                'gp_level',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Gamer\s Paradise Level'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('customer_entity'),
                'gp_points',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Gamer\s Paradise Points'
                ]
            );    


            $customerSetup->addAttribute(
                'customer',
                'gp_token',
                [
                    'label' => 'Gamer\s Paradise Token',
                    'required' => 0,
                    'visible' => 1,
                    'input' => 'text',
                    'type' => 'static',
                    'system' => 0, // <-- important, to have the value be saved
                    'position' => 40,
                    'sort_order' => 40
                ]
            );

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'gp_token')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
            ]);

            $attribute->save();


            $customerSetup->addAttribute(
                'customer',
                'gp_level',
                [
                    'label' => 'Custom Attribute',
                    'required' => 0,
                    'visible' => 1, //<-- important, to display the attribute in customer edit
                    'input' => 'text',
                    'type' => 'static',
                    'system' => 0, // <-- important, to have the value be saved
                    'position' => 40,
                    'sort_order' => 40
                ]
            );

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'gp_level')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
            ]);

            $attribute->save();


            $customerSetup->addAttribute(
                'customer',
                'gp_points',
                [
                    'label' => 'Custom Attribute',
                    'required' => 0,
                    'visible' => 1, //<-- important, to display the attribute in customer edit
                    'input' => 'text',
                    'type' => 'static',
                    'system' => 0, // <-- important, to have the value be saved
                    'position' => 40,
                    'sort_order' => 40
                ]
            );

            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'gp_points')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
            ]);

            $attribute->save();


            $setup->endSetup();



        }
    }