<?php
return [
    'cache_types' => [
        'compiled_config' => 1,
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'full_page' => 1,
        'translate' => 1,
        'config_webservice' => 1
    ],
    'backend' => [
        'frontName' => 'admin_1hw23u'
    ],
    'crypt' => [
        'key' => '7d43196385eab7fc54128c74d1510a35'
    ],
    'db' => [
        'table_prefix' => '',
        'connection' => [
            'default' => [
                'host' => 'gamersparadise-store.c56wzm4vcwhu.us-west-2.rds.amazonaws.com',
                'dbname' => 'magento',
                'username' => 'root',
                'password' => 'phaCu9Ra!',
                'active' => '1'
            ]
        ]
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => 'production',
    'session' => [
        'save' => 'files'
    ],
    'install' => [
        'date' => 'Thu, 11 Oct 2018 02:40:14 +0000'
    ],
    'system' => [
        'default' => [
            'smile_elasticsuite_core_base_settings' => [
                'es_client' => [
                    'servers' => 'search-gamersparadise-mh5x7tgwji7nx4wdtlswrqz52a.us-west-2.es.amazonaws.com:443',
                    'enable_https_mode' => '1',
                    'enable_http_auth' => '0',
                    'http_auth_user' => '',
                    'http_auth_pwd' => ''
                ]
            ]
        ]
    ]
];
