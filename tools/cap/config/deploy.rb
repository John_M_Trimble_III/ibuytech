# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "i_Buy_Tech"
set :repo_url, "git@bitbucket.org:John_M_Trimble_III/ibuytech.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
#set :deploy_to, "/var/www/html"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

set :deploy_user, 'root'

set :magento_auth_public_key, 'b917bcf0ae35c82960fe3026c422bf20'
set :magento_auth_private_key, '7634eb161cf67d3605ed64dee2d573c0'


namespace :deploy do
  desc "Change permissions"	
  task :fix_permissions do
  	on roles(:app) do
  		execute "chown -R apache:apache /var/www/html"	
  	end
  end
end

after :deploy, 'deploy:fix_permissions'
